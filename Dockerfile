FROM tomcat
MAINTAINER YT
COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/scalatra.war
EXPOSE 8080
CMD ["catalina.sh", "run"]